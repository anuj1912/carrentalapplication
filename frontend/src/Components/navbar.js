import React from 'react';
import carrentallogo from '../Components/carrentallogo.jpg';
import {   
    Link 
  } from 'react-router-dom';
function navbar() {
    return (
        <div className="navbar">
            <div className="logo">
            <img src={carrentallogo} width="50px" height="50px" alt="carimage"/>
            </div>
            <h1 style={{marginLeft:'325px',fontWeight:'bolder',color:'#c2d6d6',fontFamily:'serif',textShadow:'2px 2px tomato'}}>CAR RENTAL SERVICES INDIA</h1>
            <div className="button1">
            <button><Link style={{ textDecoration: 'none' ,color:'black'}} to="/LoginForm">Login</Link></button>
            </div>
            <div className="button2">
            <button><Link style={{ textDecoration: 'none' ,color:'black'}} to="/RegisterForm">Register</Link></button>
            </div>
            <div className="button3">
            <button><Link style={{ textDecoration: 'none' ,color:'black'}}  to="/AboutUs">About Us</Link></button>
            </div>
        </div>
    )
}

export default navbar;
