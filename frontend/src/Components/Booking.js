import axios from 'axios';
import React, { Component } from 'react'
import carrentallogo from '../Components/carrentallogo.jpg';
import Navbar1 from "./navbar1";
class Booking extends Component {
    constructor(props) {
            super(props);
            this.state = {
              CarId: '',
              Registered_Email_Id: '',
              destination: '',
              start_date: '',
              end_date: ''
            }
            this.postData=this.postData.bind(this);
        }

        onIdChange=(e)=>{
            this.setState({
                CarId:e.target.value
            })
        }
        onEmailChange=(e)=>{
            this.setState({
                Registered_Email_Id:e.target.value
            })
        }
        onDestinationChange=(e)=>{
            this.setState({
                destination:e.target.value
            })
        }
        onStartChange=(e)=>{
            this.setState({
                start_date:e.target.value
            })
        }
        onEndChange=(e)=>{
            this.setState({
                end_date:e.target.value
            })
        }
      
        validateForm(){
            //console.log("hI mundall bhiya");
            return this.state.CarId.length>0 && this.state.Registered_Email_Id.length>0 && this.state.destination.length>0 && this.state.end_date.length>0 && this.state.start_date.length>0;
        
    }
         postData(e){

        e.preventDefault();
       axios.get("http://localhost:8080/carRentals/getCarDetailByIdAndStatus/"+this.state.CarId)
        .then((response)=>{
            if(response.status==200)
            {
                axios.get("http://localhost:8080/carRentals/checkUser/"+this.state.Registered_Email_Id)
                .then((response)=>
                {
                    if(response.status==200)
                    {
                        axios.post("http://localhost:8080/bookings/"+this.state.Registered_Email_Id+"/"+this.state.CarId,this.state)
                                this.props.history.push("/Confirmation");
                    }
                })
                .catch((error)=>{
                    this.props.history.push("/InvalidUser");
                })
            }
            
        })
        .catch((error)=>{
                this.props.history.push("/CarNotAvailable");
                console.log(error);
            
        })
    }
    render(){
    return (
        <div className="form">
        <Navbar1/>
        <form onSubmit={this.postData}>
            <div className="form-inner">
                <h2>Booking</h2>
                <div className="form-group">
                    <img src={carrentallogo} width="100px" height="100px" alt="carimage"/>
                </div>
                <div className="form-group">
                    <label htmlFor="CarId">Car ID :</label>
                    <input type="number" name="CarId" id="CarId" onChange={this.onIdChange} />
                </div>
                <div className="form-group">
                    <label htmlFor="Registered_Email_Id">Registered Email ID :</label>
                    <input type="email" name="Registered_Email_Id" id="Registered_Email_Id" onChange={this.onEmailChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="destination">Destination :</label>
                    <input type="text" name="destination" id="destination" onChange={this.onDestinationChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="start_date">Journey Start Date :</label>
                    <input type="text" name="start_date" id="start_date" placeholder="dd/mm/yyyy" onChange={this.onStartChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="end_date">Journey End Date :</label>
                    <input type="text" name="end_date" id="end_date" placeholder="dd/mm/yyyy" onChange={this.onEndChange}/>
                </div>
                <input type="submit" value="Book Car" disabled={this.state.CarId.length<1 || this.state.Registered_Email_Id.length<1 || this.state.destination.length<1 || this.state.end_date.length<1 || this.state.start_date.length<1}/>
            </div>
        </form>
        </div>
    )
    }
}

export default Booking;
