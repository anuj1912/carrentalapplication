package com.example.carrental.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.mail.javamail.*;
@Service
public class emailService {
    @Autowired
    private JavaMailSender javaMailSender;

    public void sendEmail(String to,String body,String topic)
    {
        System.out.println("sending mail");
        SimpleMailMessage simpleMailMessage=new SimpleMailMessage();
        simpleMailMessage.setFrom("carrentalservicesind@gmail.com");
        simpleMailMessage.setTo(to);
        simpleMailMessage.setSubject(topic);
        simpleMailMessage.setText(body);
        javaMailSender.send(simpleMailMessage);
        System.out.println("mail successfully sent");
    }
}


