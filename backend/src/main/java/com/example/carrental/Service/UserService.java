package com.example.carrental.Service;

import com.example.carrental.Model.UserDetails;
import com.example.carrental.Repository.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.beans.FeatureDescriptor;
import java.util.List;
import java.util.stream.Stream;

@Service
public class UserService {
    @Autowired
    private UserRepository repo;

    public UserDetails addNewUser(@RequestBody UserDetails user){
        repo.save(user);
        return user;
    }

    public UserDetails getUserDetails(String id)
    {
        return repo.findById(id);
    }

    public UserDetails fetchUserByIdAndPassword(String id, String password) {
        return repo.findByIdAndPassword(id, password);
    }

    public List<UserDetails> getAllUserDetails() {
       return  repo.findAll();
    }

    public void deleteUser(String id) {
        repo.deleteById(id);
    }

    public String updateUser(String id, UserDetails user) {
        if(repo.findById(id)!=null)
        {
            UserDetails userdata= repo.findById(id);
            user=(UserDetails) PersistenceUtils.partialUpdate(userdata, user);
            repo.save(user);
            return "Details Changed Successfully";
        }
        else
        {
            return "error occured while changing details";
        }
    }
}
class PersistenceUtils {

    public static Object partialUpdate(Object dbObject, Object partialUpdateObject){
        String[] ignoredProperties = getNullPropertyNames(partialUpdateObject);
        BeanUtils.copyProperties(partialUpdateObject, dbObject, ignoredProperties);
        return dbObject;
    }

    private static String[] getNullPropertyNames(Object object) {
        final BeanWrapper wrappedSource = new BeanWrapperImpl(object);
        return Stream.of(wrappedSource.getPropertyDescriptors())
                .map(FeatureDescriptor::getName)
                .filter(propertyName -> wrappedSource.getPropertyValue(propertyName) == null)
                .toArray(String[]::new);
    }


}
