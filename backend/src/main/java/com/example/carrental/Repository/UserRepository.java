package com.example.carrental.Repository;

import com.example.carrental.Model.CarDetail;
import com.example.carrental.Model.UserDetails;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<UserDetails,Integer> {
    UserDetails findByIdAndPassword(String id, String password);

    UserDetails findById(String user_id);
    void deleteById(String user_id);
}
