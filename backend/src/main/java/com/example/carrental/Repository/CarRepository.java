package com.example.carrental.Repository;

import com.example.carrental.Model.CarDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface CarRepository extends JpaRepository<CarDetail,Integer> {

    CarDetail findByCarname(String carname);
  CarDetail  findByCarnameAndStatus(String carname,String status);
}
