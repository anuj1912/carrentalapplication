package com.example.carrental.Controller;



import com.example.carrental.Model.BookingDetails;
import com.example.carrental.Model.CarDetail;
import com.example.carrental.Model.UserDetails;
import com.example.carrental.Repository.BookingRepository;
import com.example.carrental.Repository.CarRepository;
import com.example.carrental.Repository.UserRepository;
import com.example.carrental.Service.emailService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@CrossOrigin(origins = "*")
@RestController
public class BookingController {

    @Autowired
    private UserRepository userrepo;
    @Autowired
    private BookingRepository bookingrepo;
    @Autowired
    private CarRepository carrepo;
    @Autowired
    private emailService mail;

    @PostMapping("/addBooking")
    public BookingDetails addBooking(@RequestBody BookingDetails booking){
        return bookingrepo.save(booking);
    }
    @PostMapping("/bookings/{user_id}/{car_id}")
    public String showBookings(@RequestBody BookingDetails booking,@PathVariable String user_id,@PathVariable int car_id)
    {
        UserDetails user=userrepo.findById(user_id);
        CarDetail car=carrepo.getOne(car_id);
        booking.doBooking(user);
        booking.doCarBooking(car);
        car.setStatus("Unavailable");
        String body="Congratulations Your Booking Is Confirmed ."+"\n"+"Details of booking are as follows . "+"\n"+"Car Name: "+car.getCarname()+"\n"+"Destination: "+booking.getDestination()+"\n"+"Journey Start Date: "+booking.getStart_date()+"\n"+"Journey End Date: "+booking.getEnd_date();
        String topic="Booking Confirmation Mail from Car Rental Services India";
        bookingrepo.save(booking);
        mail.sendEmail(user_id,body,topic);
        return "Booking Successfully";
    }
    @GetMapping("/getAllBookings")
    public List<BookingDetails> getAllBookings()
    {
        return  bookingrepo.findAll();
    }
    @PostMapping("/getID")
    public void sendCoupons(@RequestBody String body) {
        Set<String> al = new HashSet<>();
        List<BookingDetails> data = getAllBookings();
        ArrayList<String> emails=new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            al.add(data.get(i).getUser().getId());
        }
       emails.addAll(al);
        for(int i=0;i< emails.size();i++)
        {
            String msg=body.substring(18,body.length()-2);
            mail.sendEmail(emails.get(i),msg,"Coupon");
        }
    }
    }

