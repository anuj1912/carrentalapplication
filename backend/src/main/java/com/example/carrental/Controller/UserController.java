package com.example.carrental.Controller;

import com.example.carrental.Model.UserDetails;
import com.example.carrental.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/carRentals")
public class UserController {
    @Autowired
    private UserService service;
    @PostMapping("/addNewUser")
    public UserDetails addNewUser(@RequestBody UserDetails userdetails){
        return service.addNewUser(userdetails);
    }
    @PostMapping("/login")
    public  UserDetails login(@RequestBody UserDetails user) throws Exception {
        String tempEmail= user.getId();
        String tempPassword= user.getPassword();
        UserDetails details=null;
        if(tempEmail!=null || tempPassword!=null)
        {
            details=service.fetchUserByIdAndPassword(tempEmail,tempPassword);
        }
        if(details==null){
            throw new Exception("Bad Credentials");
        }
        return  details;
    }
    @GetMapping("/checkUser/{id}")
    public UserDetails checkUser(@PathVariable("id") String id ) throws Exception
    {
        UserDetails result=null;
        result=service.getUserDetails(id);
        if(result==null)
            throw new Exception("Bad Credentials");
        return result;
    }
    @GetMapping("/getAllUserDetails")
    public List<UserDetails> getAllUserDetails()
    {
        return service.getAllUserDetails();
    }

    @DeleteMapping("/deleteUser/{id}")
    public String deleteUser(@PathVariable("id") String id)
    {
         service.deleteUser(id);
         return "User deleted Successfully";
    }

    @PutMapping("/updateUserDetail/{id}")
    public String updateUserDetail(@PathVariable("id") String id,@RequestBody UserDetails user)
    {
        return service.updateUser(id,user);
    }
}
