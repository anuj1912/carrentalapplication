package com.example.carrental.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;


@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name="booking")
public class BookingDetails {
    @Id
    @GeneratedValue
    private int booking_id;
    private String start_date;
    private String end_date;
    private String destination;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk",referencedColumnName = "id")
    private UserDetails user;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_car",referencedColumnName = "id")
    private CarDetail car;

    public void doBooking(UserDetails user) {
        this.user=user;
    }
    public void doCarBooking(CarDetail car){this.car=car;}
}
