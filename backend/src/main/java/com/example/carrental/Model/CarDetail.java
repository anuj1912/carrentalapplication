package com.example.carrental.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "carDetails")
public class CarDetail {
    @Id
    private int id;
    private String carname;
    private String status;
    private double price;
    private String url;
    private String reg_no;
    private String insurance_end;
    private String p_dtype;
    private String a_mtype;
    private int seater;
    private String ac_nonac;
    @JsonIgnore
    @OneToMany(mappedBy = "car")
    private Set<CarDetail> car = new HashSet<>();
}
